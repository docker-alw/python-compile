# python-compile

Docker image with tool chain to compile Python code into static linked binary.

[![pipeline status](https://gitlab.com/docker-alw/python-compile/badges/main/pipeline.svg)](https://gitlab.com/docker-alw/python-compile/-/commits/main)

## Usage

To compile source code you have to:

* mount the source code directory as read-write-volume into the container
* set the working directory to the target of the mount
* specify `SRC_FILE` as the path to the python or pyinstaller-spec file relative to the mounted directory that should be compiled

Sample command using .spec-file:
```
cd git-repo/
docker run --rm -ti -w /app -v "${PWD}:/app" -e SRC_FILE=accointing_convert.spec registry.gitlab.com/docker-alw/python-compile:latest
```

This command will generate:

* dynamic linked binary in `./dist/` merged by pyinstaller called `SRC_FILE` without extension
* if `SRC_FILE` wasn't a .spec-file, pyinstaller generates one
* statically linked binary in `./bin/` called `SRC_FILE` with extension replaced by `_$(uname -i)` gernerated by staticx
