# vim:set ft=dockerfile:
FROM	ubuntu:focal

SHELL   ["/bin/bash", "-o", "pipefail", "-o", "errexit", "-c"]

ENV     DEBIAN_FRONTEND=noninteractive

RUN	apt-get update \
	&& apt-get install -qqy --no-install-recommends \
		build-essential \
		curl \
		gcc \
		python3-dev \
		binutils \
		python3-pip \
	&& pip3 install --no-cache-dir staticx pyinstaller patchelf-wrapper \
	&& apt-get autoremove -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

COPY	entrypoint.sh /entrypoint.sh

VOLUME	/app

CMD 	["/entrypoint.sh"]
