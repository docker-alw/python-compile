#!/bin/bash

set -euo pipefail

req_file="${REQUIREMENTS:-requirements.txt}"
req_dev_file="${REQUIREMENTS_COMPILE:-requirements_compile.txt}"
src_file="${SRC_FILE:-}"
platform="$(uname -i)"

if [ -z "${src_file}" ]; then
    printf "\e[1;31mSRC_FILE is not defined.\e[0m Check documentation: https://gitlab.com/docker-alw/python-compile/-/blob/main/README.md\n"
    exit 3
fi
if ! [ -f "${src_file}" ]; then
    printf "\e[1;31mGiven SRC_FILE '%s' does not exist or is not a file.\e[0m\n" "${src_file}"
    ls -l
    exit 4
fi

if [ -n "${DEBUG:-}" ]; then
    set -x
fi

if [ -f "${req_file}" ]; then
    pip install --no-cache-dir -r "${req_file}"
fi
if [ -f "${req_dev_file}" ]; then
    pip install --no-cache-dir -r "${req_dev_file}"
fi

dist_filename="${src_file%.*}"

pyinstaller --strip --clean --onefile "${src_file}"
staticx --loglevel INFO --strip "dist/${dist_filename}" "bin/${dist_filename}_${platform}"

chown -R "$(stat -c %u "${src_file}"):$(stat -c %g "${src_file}")" bin/ dist/ build/
